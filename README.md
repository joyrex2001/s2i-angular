# Angular S2I builder image

This is an Angular builder image, which will build the angular application, and
will run it with nginx webserver.

Note that this image will use the [epel repository](https://fedoraproject.org/wiki/EPEL)
for installing node and nginx.

## Usage

To build a simple angular application using standalone S2I and then run the resulting image with Docker execute:

```bash
$ s2i build https://github.com/gothinkster/angular-realworld-example-app.git angular-centos7 angular-demo
$ docker run -p 8080:8080 angular-demo
```

Accessing the application:

```
$ curl 127.0.0.1:8080
```
