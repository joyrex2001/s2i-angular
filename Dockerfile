FROM openshift/base-centos7

EXPOSE 8080

LABEL maintainer="Vincent van Dam <vincentd@erg.verweg.com>"

ENV BUILDER_VERSION 1.0

LABEL io.k8s.description="Platform for building and running Angular applications using nginx" \
      io.k8s.display-name="Angular/nginx" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,angular,nginx,centos7,frontend,http"

COPY ./s2i/bin/ /usr/libexec/s2i

## Install nginx, node and angular-cli

RUN yum -y install epel-release  && \
    yum -y install nginx         && \
    yum -y install npm           && \
    npm install -g ng-cli        && \
    yum clean all -y

## custom nginx configuration to run as unprivileged user

COPY config/nginx.conf /etc/nginx/nginx.conf

## When nginx is installed, a nginx user and group are added, these will be
## used as the user-id/group-id for this image. Make sure the right privileges
## are set on the folders used.

RUN chown -R 998:996 /opt/app-root           && \
    chown -R 998:996 /usr/share/nginx/html/  && \
    mkdir /var/run/nginx/                    && \
    chown 998:996 /var/run/nginx/            && \
    rm -rf /usr/share/nginx/html/*

USER 998

CMD ["/usr/libexec/s2i/usage"]
